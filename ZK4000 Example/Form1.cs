﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using AxZKFPEngXControl;

namespace ZK4000_Example
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int fpCache = 0;
        private int fpInit = 0;
        private bool isRegistering;
        private bool isVerifying;
        private int verType = 0;
        private object aTemplate;

        private void Form1_Load(object sender, EventArgs e)
        {
            //Check if the device is connected
            if (axZKFPEngX1.InitEngine() == 0)
            {
                label1.Text = "connected";
            }
            //Set enroll count to N number
            axZKFPEngX1.EnrollCount = 3;
            fpInit = 1;
            fpCache = axZKFPEngX1.CreateFPCacheDBEx();
            axZKFPEngX1.CancelCapture();
        }

        private void btnEnroll_Click(object sender, EventArgs e)
        {
            isRegistering = true;

            btnCancel.Enabled = true;
            btnVerify11.Enabled = false;
            btnVerify1N.Enabled = false;

            axZKFPEngX1.BeginCapture();
            axZKFPEngX1.BeginEnroll();

            label1.Text = "Ponga su huella";
        }

        private void axZKFPEngX1_OnEnroll(object sender, IZKFPEngXEvents_OnEnrollEvent e)
        {
            var sRegTemplate = axZKFPEngX1.GetTemplateAsStringEx("9");
            var sRegTemplate10 = axZKFPEngX1.GetTemplateAsStringEx("10");

            if (sRegTemplate.Length > 0)
            {
                if (sRegTemplate10.Length > 0)
                {
                    axZKFPEngX1.AddRegTemplateStrToFPCacheDBEx(fpCache, fpInit, axZKFPEngX1.GetTemplateAsStringEx("9"),
                        axZKFPEngX1.GetTemplateAsStringEx("10"));
                }
                else
                {
                    MessageBox.Show("Error al registrar huella", "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                aTemplate = axZKFPEngX1.GetTemplate();
                axZKFPEngX1.SaveTemplate("tpl.tpl", aTemplate);

                fpInit = fpInit + 1;

                axZKFPEngX1.SaveJPG("huella.jpg");
                using (FileStream img = new FileStream("huella.jpg", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    Image loaded = new Bitmap(img);
                    pictureBox2.BackgroundImage = loaded;
                    pictureBox2.BackgroundImageLayout = ImageLayout.Stretch;
                    pictureBox2.Refresh();
                }
            }
            
            axZKFPEngX1.CancelEnroll();
            MessageBox.Show("Succeed");

            isRegistering = false;
            btnCancel.Enabled = false;
            btnVerify11.Enabled = true;
            btnVerify1N.Enabled = true;
        }

        private void axZKFPEngX1_OnCapture(object sender, IZKFPEngXEvents_OnCaptureEvent e)
        {
            if (isVerifying)
            {
                switch (verType)
                {
                    case 1:
                        bool t = false;
                        var newFinger = axZKFPEngX1.GetTemplate();
                        MessageBox.Show(axZKFPEngX1.VerFinger(ref aTemplate, newFinger, false, ref t) ? "éxito" : "falló");
                        isVerifying = false;
                        axZKFPEngX1.CancelCapture();
                        break;
                    case 2:
                        int score = 8;
                        int processNumber = 0;

                        var newFinger2 = axZKFPEngX1.GetTemplate();
                        int id = axZKFPEngX1.IdentificationInFPCacheDB(fpCache, newFinger2, ref score, ref processNumber);

                        MessageBox.Show(id == -1 ? "Falló" : "Éxito!");
                        axZKFPEngX1.CancelCapture();
                        break;
                    default:
                        break;
                }

            }

            axZKFPEngX1.SaveJPG("huellaTemp.jpg");
            using (FileStream img = new FileStream("huellaTemp.jpg",FileMode.Open,FileAccess.Read, FileShare.ReadWrite))
            {
                Image loaded = new Bitmap(img);
                pictureBox1.BackgroundImage = loaded;
                pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
                pictureBox1.Refresh();
            }
        }

        private void axZKFPEngX1_OnImageReceived(object sender, IZKFPEngXEvents_OnImageReceivedEvent e)
        {
            
        }

        private void axZKFPEngX1_OnFingerTouching(object sender, EventArgs e)
        {
            if (isRegistering)
            {
                axZKFPEngX1.SaveJPG("huellaTemp.jpg");
                using (FileStream img = new FileStream("huellaTemp.jpg", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    try
                    {
                        Image loaded = new Bitmap(img);
                        pictureBox2.BackgroundImage = loaded;
                        pictureBox2.BackgroundImageLayout = ImageLayout.Stretch;
                        pictureBox2.Refresh();
                    }
                    catch (Exception)
                    {
                        
                    }
                }
            }
            else
            {
                aTemplate = axZKFPEngX1.GetTemplate();
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            axZKFPEngX1.CancelEnroll();
            btnCancel.Enabled = false;
            btnVerify11.Enabled = true;
            btnVerify1N.Enabled = true;
            isRegistering = false;
        }

        private void btnVerify11_Click(object sender, EventArgs e)
        {
            verType = 1;
            verify();
            btnEnroll.Enabled = true;
            btnCancel.Enabled = true;
        }

        private void btnVerify1N_Click(object sender, EventArgs e)
        {
            verType = 2;
            verify();
            btnEnroll.Enabled = true;
            btnCancel.Enabled = true;
        }

        private void verify()
        {
            byte[] bTemplate;

            if (aTemplate != null)
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (MemoryStream ms = new MemoryStream())
                {
                    bf.Serialize(ms, aTemplate);
                    bTemplate = ms.ToArray();
                }
                if (bTemplate.Length <= 200)
                {
                    MessageBox.Show("Ponga su huella primero...");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Ponga su huella primero...");
                return;
            }

            btnEnroll.Enabled = false;

            axZKFPEngX1.CancelEnroll();
            axZKFPEngX1.BeginCapture();
            MessageBox.Show("Ponga su huella...");

            isVerifying = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
